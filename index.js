const express = require('express');
const morgan = require('morgan');
const filesRouter = require('./filesRouter');
const { createFilesRoot } = require('./middlewares');
const app = express();

const PORT = 8080;

app.use(express.json());
app.use(morgan('tiny'));

createFilesRoot();

app.use('/api/files', filesRouter);
app.use((req, res) => res.status(404).json({ message: 'Client error' }));

app.listen(PORT, () => {
  console.log(`Server has been worked on ${PORT}...`);
});