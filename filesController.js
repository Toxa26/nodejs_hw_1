const fs = require('fs');
const fsPromises = fs.promises;

const regexFile = /\S+\.(log|txt|json|yaml|xml|js)$/i;

const getFiles = async (req, res) => {
  try {
    const files = await fsPromises.readdir(`${__dirname}/files`);
  
    res.status(200).json({ message: 'Success', files: files.filter(item => item.match(regexFile)) });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
}

const createFile = async (req, res) => {
  const { filename, content } = req.body;

  try {
    await fsPromises.writeFile(`./files/${filename}`, content, 'utf-8');
    res.status(200).json({ message: 'File created successfully' });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
}

const getFile = async (req, res) => {
  const currFile = req.params.filename;

  try {
    const content = await fsPromises.readFile(`./files/${currFile}`, 'utf8');
    const uploadedDate = (await fsPromises.stat(`./files/${currFile}`)).birthtime;
  
    res.status(200).json({
      message: 'Success', 
      filename: currFile,
      content: content,
      extension: currFile.substring(currFile.length, currFile.lastIndexOf('.') + 1),
      uploadedDate: uploadedDate
    });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
}

const modifyFile = async (req, res) => {
  const currFile = req.params.filename;
  const content = req.body.content;

  try {
    await fsPromises.writeFile(`./files/${currFile}`, content, 'utf-8');
    res.status(200).json({ message: `File's content has been changed on '${content}'` });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
}

const deleteFile = async (req, res) => {
  const currFile = req.params.filename;
  const currPass = req.query.password;

  try {
    if (currPass) {
      const passwordsDB = JSON.parse(await fsPromises.readFile(`./passwords.json`, 'utf8'));
      const filteredDB = passwordsDB.filter(item => item.filename !== currFile);

      await fsPromises.writeFile(`./passwords.json`, `${JSON.stringify(filteredDB)}`, 'utf-8');
    }

    await fsPromises.unlink(`./files/${currFile}`);
    res.status(200).json({ message: 'File deleted successfully' });
  } catch (err) {
    res.status(500).json({ message: 'Server error' });
  }
}

module.exports = {
  getFiles,
  createFile,
  getFile,
  modifyFile,
  deleteFile
}